pragma solidity >=0.4.21 <0.6.0;

contract ProductFactory {
    
    event NewProduct(uint productId, string name, uint snumber, string date, string description);
    uint snumberDigits = 10;
    uint snumberModulus = 10 ** snumberDigits;
    
    struct Product {
        string name;
        uint snumber;
        string date;
        string description;
    }
    Product[] public products;
   
   function createProduct(string memory _name, uint _snumber,string memory _date,string memory _description)  public {
       
       
        uint id = products.push(Product(_name, _snumber,_date,_description)) - 1;
        emit NewProduct(id,_name,_snumber,_date,_description);
    }
}